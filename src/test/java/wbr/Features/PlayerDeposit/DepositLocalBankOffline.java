package wbr.Features.PlayerDeposit;

import config.Annotations.Browser;
import config.BaseTest.BrowserTest;
import config.Driver.BrowserDimension;
import config.Driver.Browsers;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Narrative;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import wbr.Steps.PlayerDeposit.DepositLocalBankOfflineSteps;

@Narrative(text={"In order to play game",
        "As a player",
        "I want to deposit by Local Bank Transfer"})
@Browser(use = Browsers.CHROME, dimension = BrowserDimension.FULLSCREEN)
@RunWith(SerenityRunner.class)
public class DepositLocalBankOffline extends BrowserTest {

    @Steps
    DepositLocalBankOfflineSteps depositLocalBankOfflineSteps;

    @Test
    public void userDepositLocalBankScenario_1() throws InterruptedException {
        depositLocalBankOfflineSteps
                .userGoToLocalBankTransferPage()
//                .verifyUserIsInLocalBankTransferPage()
                .enterAmountAndClickOnContinueButton("20")
                .confirmDepositDetails()
                .confirmDepositRequestIsProcessed()
                .loginToBossAndApprove("rainbow", "123456")
                .openPlayerSiteAndVerifyDepositNumber();
    }

}
