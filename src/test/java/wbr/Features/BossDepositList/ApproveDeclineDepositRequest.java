package wbr.Features.BossDepositList;

import config.BaseTest.BrowserTest;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.junit.annotations.TestData;
import org.junit.Test;
import org.junit.runner.RunWith;
import utils.TestModel;
import utils.YamlReader;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SerenityParameterizedRunner.class)
public class ApproveDeclineDepositRequest {

    private TestModel testModel;
    public ApproveDeclineDepositRequest(TestModel testModel){
        this.testModel = testModel;
    }
    @TestData
    public static Collection<Object[]> testData() throws IOException {
        return Arrays.stream(YamlReader.readYml(TestModel.class))
                .map(it -> new Object[]{it})
                .collect(Collectors.toList());
    }


    @Test
    public void test() {
        System.out.println(testModel.getAmount());
    }



}
