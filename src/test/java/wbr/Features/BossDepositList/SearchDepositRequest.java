package wbr.Features.BossDepositList;

import config.BaseTest.BossTest;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Narrative;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.TestData;
import org.junit.Test;
import org.junit.runner.RunWith;
import utils.TestModel;
import utils.YamlReader;
import wbr.Models.SearchDepositRequestModel;
import wbr.Steps.BossDepositList.SearchDepositRequestSteps;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Narrative(text={"Search deposit requests",
        "As a payment officer",
        "I can search deposit requests by defined conditions"})
@RunWith(SerenityParameterizedRunner.class)
public class SearchDepositRequest extends BossTest {

    private SearchDepositRequestModel testModel;
    public SearchDepositRequest(SearchDepositRequestModel model){
        this.testModel = model;
    }
    @TestData
    public static Collection<Object[]> testData() throws IOException {
        return Arrays.stream(YamlReader.readYml(SearchDepositRequestModel.class))
                .map(it -> new Object[]{it})
                .collect(Collectors.toList());
    }

    @Steps
    SearchDepositRequestSteps searchDepositRequestSteps;

    @Test
    public void searchDepositByFilter() throws InterruptedException {
        searchDepositRequestSteps.goToDepositPage()
                .enterDepositStatusAndClickSearch(testModel.getDepositStatus())
                .verifyTheDepositIsFound();

    }
}
