package wbr.PageObjects;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("/en/player-center/deposit")
public class PlayerDepositPage extends PageObject {

    PlayerLocalBankTransferPage playerLocalBankTransferPage;

    @FindBy(xpath = "//div[contains(text(),'Local Bank Transfer')]")
    WebElementFacade localBankTransfer;

    public PlayerLocalBankTransferPage clickOnLocalBankTransfer() {

        localBankTransfer.waitUntilVisible().click();
        return playerLocalBankTransferPage;
    }
}
