package wbr.PageObjects;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;

@DefaultUrl("/en/player-center/deposit")
public class PlayerLocalBankTransferPage extends PageObject {

    @FindBy(xpath = "//*/button/span[contains(text(),'CONTINUE')]")
    public WebElementFacade continueButton;

    @FindBy(xpath = "//*/button/span[contains(text(),'CONFIRM')]")
    public WebElementFacade confirmButton;

    @FindBy(xpath = "//h1[contains(text(),'Local Bank Deposit')]")
    public WebElementFacade localBankDepositHeader;

    public PlayerLocalBankTransferPage enterAmount(String amount) throws InterruptedException {
        withTimeoutOf(20, SECONDS)
                .find(By.cssSelector("[placeholder=\"Amount\"]"))
                .sendKeys(amount);
        return this;
    }

    public PlayerLocalBankTransferPage clickOnViewStatusIfSuccess() {

//        WebElementFacade p = find(By.cssSelector(".notification-message-box")).find(By.tagName("p")).selectByIndex(0);
        waitForTextToAppear("Your deposit request is being processed.");

        find(By.xpath("//span[contains(text(),'VIEW STATUS')]")).waitUntilPresent().click();
        return this;
    }

    public String getDepositNumber() {
        WebElementFacade depositNumberTd =  find(By.xpath("//table/tbody/tr[1]/td[2]")).waitUntilPresent();
        return depositNumberTd.getText();
    }

    public PlayerLocalBankTransferPage clickOnContinueButton() throws InterruptedException {
        continueButton.click();
        return this;
    }





}
