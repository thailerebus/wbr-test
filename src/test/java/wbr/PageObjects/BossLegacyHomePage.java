package wbr.PageObjects;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("/auth/login")
public class BossLegacyHomePage extends PageObject {

//    public BossLegacyLoginPage(WebDriver driver) {
//        super(driver);
//    }
    BossDepositListPage bossDepositListPage;



    public BossDepositListPage clickOnPaymentTab()  {
        find(By.cssSelector("[href=\"/go_to/new_boss?page=payment-management/deposit-list\"]")).waitUntilPresent().click();

        return bossDepositListPage;
    }

}
