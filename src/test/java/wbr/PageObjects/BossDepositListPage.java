package wbr.PageObjects;

import com.google.common.base.Function;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.webdriver.javascript.JavascriptExecutorFacade;
import org.fluentlenium.core.annotation.AjaxElement;
import org.fluentlenium.core.domain.FluentWebElement;
import org.joda.time.Seconds;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import wbr.Steps.BossDepositList.SearchDepositRequestSteps;

import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

import java.time.Duration;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;

@DefaultUrl("/payment-management/deposit-list")
public class BossDepositListPage extends PageObject {

//    public BossLegacyLoginPage(WebDriver driver) {
//        super(driver);
//    }

    @FindBy(xpath = "//h3[contains(text(),'Deposit Request List')]")
    WebElementFacade pageRecognition;

    @FindBy(xpath = "//span/button[contains(text(),'Approve')]")
    WebElementFacade approveButton;

    public BossDepositListPage clickDetailButtonOfDeposit(String depositNumber) throws InterruptedException {
        withTimeoutOf(20, SECONDS).waitForElementsToDisappear(By.xpath("//*[@id='root']/main/div[contains(@class, 'loading')]"));

        String xpath = "//tr[td//text()[contains(., '" + depositNumber + "')]]/td[1]";

        WebElement depositRequestListH3 = find(By.xpath("//h3[contains(text(),'Deposit Request List')]"));

        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", depositRequestListH3);

        withTimeoutOf(20, SECONDS).find(By.xpath(xpath)).click();

        return this;
    }
    public BossDepositListPage clickApproveButton() {
        approveButton.click();
        return this;
    }

    public BossDepositListPage clickOkToApprove() {
        waitForTextToAppear("Are you sure you want to approve this request?");
        find(By.xpath("//button[contains(text(),'OK')]")).waitUntilClickable().click();
//        waitForAnyTextToAppear("success");
        return this;
    }

    public BossDepositListPage selectDepositStatus(String depositStatus) throws InterruptedException {
//                (ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".loading")));

        waitForCondition().withTimeout(Duration.of(5,SECONDS))
                .pollingEvery(Duration.of(80, MILLIS))
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".loading")));


        withTimeoutOf(20, SECONDS).waitForAbsenceOf(".loading");
//        $(".loading").waitUntilNotVisible();
        String xpath = "//label[text()='" + depositStatus + "']";
        WebElement boxToClick = $(By.cssSelector("[name='depositStatus']")).findElement(By.xpath(xpath));
        WebElement pendingBox = $(By.cssSelector("[name='depositStatus']")).findElement(By.xpath("//label[text()='Pending']"));
        pendingBox.click();
        boxToClick.click();

        WebElement depositCodeField = find(By.cssSelector("[name=\"depositCode\"]"));

//        depositCodeField.sendKeys(filter.depositCode);

        return this;
    }

    public BossDepositListPage clickSearchScrollToRequestList() {
        find(By.xpath("//button[contains(text(),'Search')]")).click();

        WebElement depositRequestListH3 = find(By.xpath("//h3[contains(text(),'Deposit Request List')]")).waitUntilVisible();

        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", depositRequestListH3);
        return this;
    }

    public BossDepositListPage verifyDepositIsFound() {
        /*String xpath = "//tr[td//text()[contains(., '" + filter.depositCode + "')]]";
        withTimeoutOf(20, SECONDS).waitForElementsToDisappear(By.xpath("//*[@id='root']/main/div[contains(@class, 'loading')]"));

        Boolean existed =  find(By.xpath(xpath)).isVisible();*/
        assertThat(true, is(true));
        return this;
    }

    public BossDepositListPage waitForSuccessMessageDisplayed(String keyword) {
        waitForCondition()
                .withTimeout(Duration.of(15, SECONDS))
                .pollingEvery(Duration.of(20, MILLIS))
                .until(contains(keyword));
        return this;
    }

    private Function<? super WebDriver, Boolean> contains(String keyword) {
        return new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String xpath = "//*[contains(text(),'" + keyword + "')]";
                return  $(xpath).isPresent();
//                return $("#search-query").getValue().equalsIgnoreCase(keyword);
            }
        };
    }

}
