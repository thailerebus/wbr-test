package wbr.PageObjects;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;


@DefaultUrl("/en/player-center/transactions/deposit")
public class PlayerDepositTransactionsPage extends PageObject {

    public PlayerDepositTransactionsPage verifyDepositIsSuccess(String depositCode) {

        String xpath = "//tr[td//text()[contains(., '" + depositCode + "')]]/td[4]";

        String status = withTimeoutOf(20, SECONDS).find(By.xpath(xpath)).getText();

        assertThat(status, is("Success"));
        return this;
    }

}
