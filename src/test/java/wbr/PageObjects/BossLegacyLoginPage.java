package wbr.PageObjects;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("/auth/login")
public class BossLegacyLoginPage extends PageObject {

//    public BossLegacyLoginPage(WebDriver driver) {
//        super(driver);
//    }
    BossLegacyHomePage bossLegacyHomePage;

    @FindBy(css = "[name=\"login\"]")
    WebElementFacade login;

    @FindBy(css = "[name=\"password\"]")
    WebElementFacade password;

    @FindBy(css = "[name=\"submit\"]")
    WebElementFacade submitButton;

    public BossLegacyHomePage loginToBoss(String username, String password) {
        this.login.sendKeys(username);
        this.password.sendKeys(password);
        this.submitButton.click();
        return bossLegacyHomePage;
    }

}
