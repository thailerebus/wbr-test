package wbr.PageObjects;

import com.google.common.base.Function;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

import static java.time.temporal.ChronoUnit.SECONDS;

public class BaseBossPage extends PageObject {
    public void waitForPageFinishToLoad() {
        waitForCondition()
                .withTimeout(Duration.of(25, SECONDS))
                .until(loadBoxDisappear());
    }

    private Function<? super WebDriver, Boolean> loadBoxDisappear() {
        return new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                String xpath = "//*[@id='root']/main/div[contains(@class, 'loading')]";

                return  !$(xpath).isVisible();
//                return $("#search-query").getValue().equalsIgnoreCase(keyword);
            }
        };
    }
}
