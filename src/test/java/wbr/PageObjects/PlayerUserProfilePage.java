package wbr.PageObjects;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("/en")
public class PlayerUserProfilePage extends PageObject {

    PlayerDepositPage playerDepositPage;

    @FindBy(xpath = "//span[contains(text(),'Transfer')]")
    public WebElementFacade transferTab;

    @FindBy(xpath = "//span[contains(text(),'Deposit')]")
    public WebElementFacade depositTab;

    @FindBy(xpath = "//span[contains(text(),'My Transactions')]")
    public WebElementFacade transactionTab;

    public PlayerUserProfilePage clickOnTransferTab() {
        transferTab.click();
        return this;
    }

    public PlayerUserProfilePage clickOnMyTransactionTab() {
        transactionTab.click();
        return this;
    }

    public PlayerDepositPage clickOnDepositTab() {
        depositTab.click();
        return playerDepositPage;
    }
}
