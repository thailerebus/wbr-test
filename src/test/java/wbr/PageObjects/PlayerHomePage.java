package wbr.PageObjects;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static java.time.temporal.ChronoUnit.SECONDS;

@DefaultUrl("/en")
public class PlayerHomePage extends PageObject {
    /*private EnvironmentVariables environmentVariables;

    String playerBaseUrl =  EnvironmentSpecificConfiguration.from(environmentVariables)
            .getProperty("player.base.url");*/

    PlayerUserProfilePage playerUserProfilePage;

    @FindBy(css = "input[placeholder=\"Username\"]")
    public WebElementFacade userName;

    @FindBy(css = "input[placeholder=\"Password\"]")
    public WebElementFacade password;

    @FindBy(xpath = "//span[contains(text(),'Login')]")
    public WebElementFacade loginButton;

    public PlayerUserProfilePage clickOnUserProfileLink() {
        find(By.cssSelector("a[href=\"/en/player-center/account/profile/\"]")).click();
        return playerUserProfilePage;

    }

    public PlayerHomePage loginByUsernameAndPassword(String username, String password) {
        this.userName.sendKeys(username);
        this.password.sendKeys(password);
        this.loginButton.click();
        return this;
    }

    public PlayerHomePage waitForPageToLoadAfterLogin() {
        withTimeoutOf(20, SECONDS).find(By.xpath("//*[@id=\"root\"]/header/div[2]/div/div[1]/a"));

        return this;
    }
}
