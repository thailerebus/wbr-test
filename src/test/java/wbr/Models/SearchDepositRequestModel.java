package wbr.Models;

public class SearchDepositRequestModel {

     String depositStatus ;
     String depositCode;

     public String getDepositCode() {
          return depositCode;
     }

     public void setDepositCode(String depositCode) {
          this.depositCode = depositCode;
     }

     public String getDepositStatus() {
          return depositStatus;
     }

     public void setDepositStatus(String depositStatus) {
          this.depositStatus = depositStatus;
     }
}
