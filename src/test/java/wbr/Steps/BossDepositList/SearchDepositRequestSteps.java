package wbr.Steps.BossDepositList;

import net.thucydides.core.annotations.Step;
import wbr.Models.SearchDepositRequestModel;
import wbr.PageObjects.BossDepositListPage;
import wbr.Steps.BaseSteps;

public class SearchDepositRequestSteps extends BaseSteps {
    BossDepositListPage bossDepositListPage;
    Filter filter = new Filter();



    @Step
    public SearchDepositRequestSteps goToDepositPage(){


        bossDepositListPage = bossLegacyHomePage.clickOnPaymentTab();

        return this;
    }
    @Step
    public SearchDepositRequestSteps enterDepositStatusAndClickSearch(String depositStatus) throws InterruptedException {

        bossDepositListPage.selectDepositStatus(depositStatus)
                .clickSearchScrollToRequestList();

        return this;

    }

    @Step
    public SearchDepositRequestSteps verifyTheDepositIsFound(){
        bossDepositListPage.verifyDepositIsFound();
        return this;
    }

    public final class Filter {

        public Filter() {
            this.depositStatus = "All";
            this.depositCode = "D320175104092";
        }
        public String depositStatus;
        public String transactionPeriod;
        public String depositCode;
        public String username;
        public String currency;
        public String paymentType;
        public String collectionAccountName;
    }

}
