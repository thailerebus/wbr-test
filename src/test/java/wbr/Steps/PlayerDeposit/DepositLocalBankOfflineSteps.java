package wbr.Steps.PlayerDeposit;

import net.serenitybdd.core.environment.ConfiguredEnvironment;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.PageUrls;
import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.configuration.SystemPropertiesConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.webdriver.Configuration;
import org.openqa.selenium.WebDriver;
import wbr.PageObjects.*;
import wbr.Steps.BaseSteps;

import java.time.temporal.ChronoUnit;

import static net.serenitybdd.core.Serenity.getWebdriverManager;
import static net.serenitybdd.core.Serenity.takeScreenshot;

public class DepositLocalBankOfflineSteps extends BaseSteps {

    private EnvironmentVariables testEnv;

    PlayerHomePage playerHomePage;
    PlayerLocalBankTransferPage playerLocalBankTransferPage;
    PlayerDepositTransactionsPage playerDepositTransactionsPage;
    BossDepositListPage bossDepositListPage;
    String depositNumber;

    @Step
    public DepositLocalBankOfflineSteps userGoToLocalBankTransferPage() {

        playerLocalBankTransferPage = playerHomePage
                .clickOnUserProfileLink()
                .clickOnDepositTab()
                .clickOnLocalBankTransfer();
        return this;
    }

    @Step
    public DepositLocalBankOfflineSteps verifyUserIsInLocalBankTransferPage() {
        playerLocalBankTransferPage
                .localBankDepositHeader.waitUntilVisible().withTimeoutOf(20, ChronoUnit.SECONDS);

        return this;
    }

    @Step
    public DepositLocalBankOfflineSteps enterAmountAndClickOnContinueButton(String amount) throws InterruptedException {
        playerLocalBankTransferPage.enterAmount(amount)
                .clickOnContinueButton();
        return this;
    }

    @Step
    public DepositLocalBankOfflineSteps confirmDepositDetails() {

        //Assert here
        playerLocalBankTransferPage.confirmButton.click();


        return this;
    }

    @Step
    public DepositLocalBankOfflineSteps confirmDepositRequestIsProcessed(){
        playerLocalBankTransferPage.clickOnViewStatusIfSuccess();
        depositNumber = playerLocalBankTransferPage.getDepositNumber();
//        depositNumber = "D594812417090";

        System.out.println("thai " + depositNumber);
        return this;

    }

    @Step
    public DepositLocalBankOfflineSteps loginToBossAndApprove(String username, String password) throws InterruptedException {
        loginBossByUsernameAndPassword(username,password);

        bossLegacyHomePage
                .clickOnPaymentTab()
                .clickDetailButtonOfDeposit(depositNumber)
                .clickApproveButton()
                .clickOkToApprove()
                .waitForSuccessMessageDisplayed("successful");
        return this;
    }

    @Step
    public DepositLocalBankOfflineSteps openPlayerSiteAndVerifyDepositNumber() {
         playerDepositTransactionsPage.setPageUrls(getPageUrl(playerDepositTransactionsPage, playerBaseUrl));
         playerDepositTransactionsPage.open();
         playerDepositTransactionsPage.verifyDepositIsSuccess(depositNumber);

        return this;
    }


}
