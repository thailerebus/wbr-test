package wbr.Steps;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.PageUrls;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.configuration.SystemPropertiesConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import net.thucydides.core.webdriver.Configuration;
import wbr.PageObjects.BossLegacyHomePage;
import wbr.PageObjects.BossLegacyLoginPage;
import wbr.PageObjects.PlayerHomePage;

public class BaseSteps {

    static final String DEFAULT_BASE_URL_PROP = "environments.default.webdriver.base.url";
    static final String STAGING_BASE_URL_PROP = "environments.staging.webdriver.base.url";
    static final String TEST_BASE_URL_PROP = "environments.test.webdriver.base.url";

    static final String BOSS_LEGACY_BASE_URL_PROP = "boss.legacy.base.url";
    static final String PLAYER_BASE_URL_PROP = "player.base.url";
    static final String BOSS_BASE_URL_PROP = "boss.base.url";

    static final String STAGING = "staging";
    static final String TEST = "dev";

    public String playerBaseUrl;
    public String bossLegacyBaseUrl;

    public BossLegacyLoginPage bossLegacyLoginPage;
    public BossLegacyHomePage bossLegacyHomePage;
    public PlayerHomePage playerHomePage;

    public EnvironmentVariables testEnv = SystemEnvironmentVariables.createEnvironmentVariables();



    public BaseSteps() {
        this.playerBaseUrl = EnvironmentSpecificConfiguration.from(testEnv).getProperty(PLAYER_BASE_URL_PROP);
        this.bossLegacyBaseUrl =  EnvironmentSpecificConfiguration.from(testEnv).getProperty(BOSS_LEGACY_BASE_URL_PROP);
    }

    @Step
    public BaseSteps loginFrontendByUsernameAndPassword(String username, String password) {

        playerHomePage.setPageUrls(getPageUrl(playerHomePage, playerBaseUrl));

        playerHomePage.open();
        playerHomePage.loginByUsernameAndPassword("cny3stg","rebus@123")
                .waitForPageToLoadAfterLogin();
        return this;
    }

    @Step
    public BaseSteps loginBossByUsernameAndPassword(String username, String password) {

//        https://github.com/serenity-bdd/serenity-core/issues/1795
        bossLegacyLoginPage.setPageUrls(getPageUrl(bossLegacyLoginPage, bossLegacyBaseUrl));
        bossLegacyLoginPage.open();
        bossLegacyHomePage = bossLegacyLoginPage.loginToBoss(username, password);

        return this;
    }

    public PageUrls getPageUrl(PageObject page,String url) {

        String env = testEnv.getProperty("environment");

        if (env != null) {
            switch (env) {
                case STAGING:
                    testEnv.setProperty(STAGING_BASE_URL_PROP, url);
                    break;
                case TEST:
                    testEnv.setProperty(TEST_BASE_URL_PROP, url);
                    break;
                default:
                    break;
            }
        }

        else {
            testEnv.setProperty(DEFAULT_BASE_URL_PROP, url);
        }

        PageUrls pageUrls = new PageUrls(page, testEnv);

        return pageUrls;
    }

}
