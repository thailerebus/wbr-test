package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;

public class YamlReader {
    public static <T> Object[] readYml(Class<T> c) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        return mapper.readValue(new File("src/test/resources/thai.yml"), mapper.getTypeFactory().constructArrayType(c));
    }
}


