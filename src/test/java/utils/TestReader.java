package utils;

import org.junit.Test;

import java.io.IOException;

public class TestReader {
    @Test
    public void test() throws IOException {
        TestModel[] t = (TestModel[]) YamlReader.readYml(TestModel.class);

        for (TestModel object : t) {
            System.out.println(object.depositCode);
        }
    }

}
