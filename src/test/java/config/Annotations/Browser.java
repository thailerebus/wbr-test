package config.Annotations;

import config.Driver.BrowserDimension;
import config.Driver.Browsers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Browser {
    Browsers use() default Browsers.CHROME;
    BrowserDimension dimension() default BrowserDimension.DEFAULT;
}