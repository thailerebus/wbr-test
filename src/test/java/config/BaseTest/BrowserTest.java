package config.BaseTest;

import config.Annotations.Browser;
import config.Driver.BrowserDimension;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import wbr.Steps.BaseSteps;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class BrowserTest {


    @Managed
    WebDriver driver;

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Steps
    BaseSteps playerLoginSteps;

    @Before
    public void openPlayerSiteAndLogin() {
        driver.manage().deleteAllCookies();
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        changeDimension();

        playerLoginSteps
                .loginFrontendByUsernameAndPassword("cny3stg", "rebus@123");


    }

    private void changeDimension() {
        Browser annotation = (Browser) this.getClass().getAnnotation(Browser.class);
        BrowserDimension dimension = annotation != null ? annotation.dimension() : BrowserDimension.DEFAULT;
        if (dimension != BrowserDimension.DEFAULT) {
            manageWindowSize(dimension);
        }
    }

    private void manageWindowSize(BrowserDimension dimension) {
        switch (dimension) {
            case SMALL:
                resizeWindowTo(359);
                break;
            case MEDIUM:
                resizeWindowTo(599);
                break;
            case LARGE:
                resizeWindowTo(959);
                break;
            case XLARGE:
                resizeWindowTo(1199);
                break;
            case XXLARGE:
                resizeWindowTo(1280);
            case FULLSCREEN:
                driver.manage().window().maximize();
                break;
            default:
                driver.manage().window().maximize();
        }
    }
    private void resizeWindowTo(Integer width) {
        Integer height = 800;
        driver.manage().window().setSize(new Dimension(width, height));
    }
}