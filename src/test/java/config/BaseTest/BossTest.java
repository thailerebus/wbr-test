package config.BaseTest;

import config.Annotations.Browser;
import config.Driver.BrowserDimension;
import config.Driver.Browsers;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.WebdriverManager;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import wbr.Steps.BaseSteps;

import java.util.concurrent.TimeUnit;

public class BossTest {

    @Managed
    WebDriver driver;

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Steps
    BaseSteps bossLegacyLoginSteps;

    @Before
    public void openBossAndLogin() throws InterruptedException {
//        driver.manage().deleteAllCookies();
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        bossLegacyLoginSteps.loginBossByUsernameAndPassword("rainbow", "123456");

    }

    @Before
    public void changeDimension() {
        Browser annotation = (Browser) this.getClass().getAnnotation(Browser.class);
        BrowserDimension demension = annotation != null ? annotation.dimension() : BrowserDimension.DEFAULT;

        if (demension != BrowserDimension.DEFAULT) {
            manageWindowSize(demension);
        }
    }

    private void manageWindowSize(BrowserDimension dimension) {
        switch (dimension) {
            case SMALL:
                resizeWindowTo(359);
                break;
            case MEDIUM:
                resizeWindowTo(599);
                break;
            case LARGE:
                resizeWindowTo(959);
                break;
            case XLARGE:
                resizeWindowTo(1199);
                break;
            case XXLARGE:
                resizeWindowTo(1280);
            case FULLSCREEN:
                driver.manage().window().maximize();
                break;
            default:
                driver.manage().window().maximize();
        }
    }
    private void resizeWindowTo(Integer width) {
        Integer height = 800;
        driver.manage().window().setSize(new Dimension(width, height));
    }

}