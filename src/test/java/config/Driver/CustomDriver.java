package config.Driver;

import config.Annotations.Browser;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class CustomDriver implements DriverSource {

    @Override
    public WebDriver newDriver() {

        Browser annotation = (Browser) this.getClass().getAnnotation(Browser.class);
        Browsers browser = annotation != null ? annotation.use() : Browsers.CHROME;
        WebDriver currentDriver = new DriverManager().get(browser);

        currentDriver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
//        currentDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        currentDriver.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);

        currentDriver.manage().window().maximize();

        return  currentDriver;
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }

}

