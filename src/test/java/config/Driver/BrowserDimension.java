package config.Driver;

public enum BrowserDimension {
    SMALL,
    MEDIUM,
    LARGE,
    XLARGE,
    XXLARGE,
    FULLSCREEN,
    MAXIMIZED,
    DEFAULT,
}
