package config.Driver;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;

public class DriverManager {

    private Boolean remoteTestSetting = false;

    public DriverManager() {

        String remotetest = System.getProperty("remoteTest");

        if (remotetest != null && remotetest.equals("true")) {
            this.remoteTestSetting = true;
        }
    }

    public WebDriver get(Browsers browser) {

        return chrome();


    }

    private WebDriver chrome()  {
        if (!remoteTestSetting) {
            WebDriverManager.chromedriver().setup();
            return new ChromeDriver(chromeOptions());
        } else {
            return remoteTest(chromeOptions());
        }
    }

    private ChromeOptions chromeOptions() {
        return new ChromeOptions().merge(capabilities());
    }

    private WebDriver remoteTest(Capabilities c) {
        try {
            return new RemoteWebDriver(new URL("http://192.168.99.100:4444/wd/hub"), c);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    private DesiredCapabilities capabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("screenResolution", "1366x768");
        capabilities.setCapability("tz", "Asia/Manila");
        return capabilities;

    }



}
