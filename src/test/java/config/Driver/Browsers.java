package config.Driver;

public enum Browsers {

    CHROME("chrome");

    private final String value;

    private Browsers(String browser) {
        this.value = browser;
    }
}
